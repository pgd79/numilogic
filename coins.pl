cointype(silver5cad, 
     [weight(1, oz), alloy(ag, 99.99)]).

coin('Maple Leaf', 5, cad, 2014, bu, silver5cad).
coin('Maple Leaf', 5, cad, 2015, bu, silver5cad).
coin('Maple Leaf', 5, cad, 2016, bu, silver5cad).

cointype(cotw200cad, 
     [weight(1, oz), alloy(au, 99.999)]).

coin('Roaring Grizzly Bear', 200, cad, 2016, bu, cotw200cad).
coin('Growling Cougar', 200, cad, 2015, bu, cotw200cad).
coin('Howling Wolf', 200, cad, 2014, bu, cotw200cad).

cointype(gold50cad, 
     [weight(1, oz), alloy(au, 99.99)]).

cointype(pt50cad, 
     [weight(1, oz), alloy(pt, 99.95)]).

cointype(pd50cad, 
     [weight(1, oz), alloy(pd, 99.95)]).

coin('Maple Leaf', 50, cad, 2016, bu, pt50cad).
coin('Maple Leaf', 50, cad, 2007, bu, pd50cad).
